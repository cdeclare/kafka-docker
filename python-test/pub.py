
from datetime import datetime
from kafka import KafkaProducer

def main():
	producer = KafkaProducer(bootstrap_servers='localhost:9092')
	for i in range(3):
		s = 'Message #{} - {}'.format(i, str(datetime.now())[0:19])
		r = producer.send('run', s.encode())
		print(s)
	producer.flush()
	producer.close()

main()


	




