
from kafka import KafkaConsumer, SimpleConsumer
from kafka import TopicPartition


def main():
	#consumer = KafkaConsumer(bootstrap_servers='localhost:9092' , auto_offset_reset='earliest'
	#consumer = KafkaConsumer(bootstrap_servers='localhost:9092' , group_id='run1') 
	consumer = KafkaConsumer(bootstrap_servers='localhost:9092' , group_id='run2') 

	#consumer.subscribe(['test'])
	pts = []
	pts.append(TopicPartition('run', 0))
	pts.append(TopicPartition('run', 1))
	pts.append(TopicPartition('run', 2))
	pts.append(TopicPartition('run', 3))

	consumer.assign(pts)
	#consumer.seek(pts[0], 5)

	for m in consumer:
		#print(m)
		print('topic:{} ({}:{}) -> {} '.format(m.topic, m.partition, m.offset, m.value))

	consumer.commit()
	consumer.close()


main()


